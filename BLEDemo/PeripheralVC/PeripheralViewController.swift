//
//  PeripheralViewController.swift
//  BLEDemo
//
//  Created by Aymen Bouzaida on 25/01/2022.
//

import UIKit
import CoreBluetooth
import Foundation
import CryptoKit
import CryptoSwift

class PeripheralViewController: UIViewController {

    //NOTE: at this step we are connected to the selected BLE Peripheral
    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
     let charIdentifierString = "00002AE3-0000-1000-8000-00805f9b34fb"
    var charIdentifierUUID: UUID?

   
    var peripheralBLE: PeripheralBLE!
    var servicesPeripheral: CBPeripheral!
    var discoveredServiceCharachtarestics: CBService!
    var idChar: CBCharacteristic!
    
    
    let packIDServiceUID = "1876"
    
    let initAuthCharID = "2AE1"
    let readAuthCharID = "2AE2"
    let charIdentifierID = "2AE3"
    let readerResultID = "2AE4"
    var nonce1: Nonce!
    
    var session = SessionBle()
    
    lazy var discoverButton: UIButton = {
        var button = UIButton()
        button.setTitle("discover services", for: .normal)
        button.backgroundColor = .systemCyan
        button.setTitleColor( UIColor.white, for: .normal)
        return button
    }()
    
    lazy var centralManager = {
       return CBCentralManager(delegate: nil, queue: nil, options: [CBCentralManagerOptionShowPowerAlertKey:true])
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        peripheralBLE.basePeripheral.delegate = self
        
        discoverButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(discoverButton)
        
        discoverButton.widthAnchor.constraint(equalToConstant: 220).isActive = true
        discoverButton.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        discoverButton.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor).isActive = true
        
        discoverButton.addTarget(self, action: #selector(didTapDiscoverServices), for: .touchUpInside)
        charIdentifierUUID = UUID(uuidString: charIdentifierString)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        titleLabel.text = "Connected to: " + (peripheralBLE.advertisedName ?? "---")
    }
    
    @objc func didTapDiscoverServices(){
        
        if peripheralBLE.basePeripheral.state == .disconnected{
            centralManager.connect(peripheralBLE.basePeripheral, options: nil)
        }else{
            discoverServices()
        }
    }
    
    func initAuthTapKey(){
        
    }
    
    func readAuthTapKEy(){
        
    }
    
    // Call after connecting to peripheral
    func discoverServices() {
       
        peripheralBLE.basePeripheral.discoverServices(nil)
    }
    
    // Call after discovering services
    func discoverCharacteristics(peripheral: CBPeripheral) {
        guard let services = peripheral.services else {
            return
        }
        for service in services {
            if service.uuid.uuidString == packIDServiceUID{
                peripheral.discoverCharacteristics(nil, for: service)
            }
        }
    }
 
    
    func write(valueString: String, characteristic: CBCharacteristic) {
        
        if characteristic.uuid.uuidString == charIdentifierID{
            
        }
        
//        guard let value = (valueString as NSString).data(using: String.Encoding.description.hexa.rawValue) else {
//            print("couldn't convert to DATA")
//            return
//        }
        
        let data  = valueString.hexaData
        print(" DATA TO SEND ", data)
        print(" DATA TO SEND ", data.bytes)

        self.peripheralBLE?.basePeripheral.writeValue(data, for: characteristic, type: .withResponse)
     }

}

extension PeripheralViewController: CBPeripheralDelegate{

    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let services = peripheral.services else {
            return
        }
        guard let openService = UUID(uuidString: "000018FA-0000-1000-8000-00805f9b34fb") else{
            return
        }

        let id = CBUUID(nsuuid: openService)
//        peripheralBLE.basePeripheral = peripheral
        self.servicesPeripheral = peripheral
        servicesPeripheral.delegate = self

        for service in services{
//            uuid.UUID("000018fa-0000-1000-8000-00805f9b34fb").hex
            let foundIt = service.uuid == id
            print(" did found service ", foundIt)
            print("service.uuid.uuidString",service.uuid.description)
//            discoverCharacteristics(peripheral: peripheral)

            if service.uuid.uuidString == packIDServiceUID{
                discoverCharacteristics(peripheral: peripheral)
            }
        }
    }
     
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard let characteristics = service.characteristics else {
            return
        }
        // Consider storing important characteristics internally for easy access and equivalency checks later.
        // From here, can read/write to characteristics or subscribe to notifications as desired.

        for char in characteristics{
            
            if char.uuid.uuidString == initAuthCharID{
                discoveredServiceCharachtarestics = service
                idChar = char
                nonce1 = Nonce(length: 8)
//                print("nonce bytes ", nonce1.bytes)
                guard let nonce1 = nonce1 else { return }
                let nonceString = String(describing: nonce1).lowercased()
                let dataToSend = String(describing: nonce1).lowercased() + HardCodedData.walletID
                print("This is a random nonce: \(String(describing: nonce1).lowercased()) of length: \(nonce1.length)")
                print("nonce 1 ", nonceString.hexaData.bytes )
                print("data to send ", dataToSend)
                write(valueString: dataToSend, characteristic: char)
            }
        }
    }
    
    // Only called if write type was .withResponse
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
       
        if let error = error {
            // Handle error
            return
        }
        // Init Auth written with no error
        if characteristic.uuid.uuidString == initAuthCharID{
            
            servicesPeripheral = peripheral
            
            if let service =  peripheral.services?.first(where: { $0.uuid.uuidString == packIDServiceUID }),
                let charReadAuth = service.characteristics?.first(where: { $0.uuid.uuidString == readAuthCharID })
            {
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    peripheral.delegate = self
//                    peripheral.readValue(for: charReadAuth)
                    peripheral.setNotifyValue(true, for: charReadAuth)
//                }
            }
        }
        // Successfully wrote value to characteristic
        if characteristic.uuid.uuidString == charIdentifierID{
           
            if let service =  peripheral.services?.first(where: { $0.uuid.uuidString == packIDServiceUID }),
               let charReadResult = service.characteristics?.first(where: { $0.uuid.uuidString == readerResultID }){
                peripheral.readValue(for: charReadResult)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
     
        print("error ", error?.localizedDescription)
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        
        // Notified Read Auth
        if characteristic.uuid.uuidString == readAuthCharID{
           
            if let data = characteristic.value {
                print(" received data ", data)
                print("received bytes" , data.bytes)
                print("data.hexadecimal", data.hexadecimal)

                let deccrypted = PeripheralViewController.decryptArtisan(cryptedData: data, key: HardCodedData.walletKey2)
                
                session.calculateSessionKey(receivedNonces: deccrypted)
                let encryptedBadgeId = session.encryptBadgeId()
                
                print("session ", session.sessionKey)
                print("encryptedBadgeId",encryptedBadgeId)

                
                if let service =  peripheral.services?.first(where: { $0.uuid.uuidString == packIDServiceUID }),
                   let charIdentifier = service.characteristics?.first(where: { $0.uuid.uuidString == charIdentifierID }){
                    
                    
                    write(valueString: encryptedBadgeId, characteristic: charIdentifier)
                }
              
            }
         
        }
        
        // READ Result Value
        if characteristic.uuid.uuidString == readerResultID
        {
            print("* ******* READ Result")
            if let data = characteristic.value {
                print("data reader result",data)
                print("data reader result",data.bytes)
                print(data , String(data: data, encoding: .utf8))
                guard let resultString  = String(data: data, encoding: .utf8) else { return }
                print("resultString",resultString)
            }
        }
    }
    
    static func decryptArtisan(cryptedData: Data , key:String) -> String? {
        var clairMessage:String? = nil;
        let iv : Array<UInt8> = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        let dataKEYYYY = key.hexaBytes
        let usedKey = Array<UInt8>(key.utf8)
        
        do {
            print("key  ", key)
            print("data Key ", dataKEYYYY)
            let aes = try AES(key: dataKEYYYY, blockMode: CBC(iv: iv), padding: .noPadding) // aes128
            let cipher = try aes.decrypt(Array<UInt8>(cryptedData))
            print("cipher", cipher)
            clairMessage = cipher.hexString
            print("cipher.hexString", cipher.hexString)
        }catch{
            let error = error as NSError
            print(error)
        }
        
        return clairMessage
    }
    
    static func aesEncrypt(key: String, dataString: String) -> String{
        let iv : Array<UInt8> = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        let dataKEYYYY = key.hexaBytes
        
        let data = dataString.hexaData
        print("inversedNonces", dataString)
        print("datat " , data)
        var encryptedData = ""
        do {
            
            let aes = try AES(key: dataKEYYYY, blockMode: CBC(iv: iv), padding: .noPadding) // aes128
            let cipher = try aes.encrypt(Array<UInt8>(data))
            encryptedData = cipher.hexString
            print("cipher encrypt ", cipher)
        }catch{
            let error = error as NSError
            print(error)
            
        }
        
        return encryptedData
    }

}

extension PeripheralViewController: CBCentralManagerDelegate{
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        self.peripheralBLE.basePeripheral = peripheral
        discoverServices()
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
    }
}

public extension Data {
    init?(hexString: String) {
      let len = hexString.count / 2
      var data = Data(capacity: len)
      var i = hexString.startIndex
      for _ in 0..<len {
        let j = hexString.index(i, offsetBy: 2)
        let bytes = hexString[i..<j]
        if var num = UInt8(bytes, radix: 16) {
          data.append(&num, count: 1)
        } else {
          return nil
        }
        i = j
      }
      self = data
    }
    /// Hexadecimal string representation of `Data` object.
    var hexadecimal: String {
        return map { String(format: "%02x", $0) }
            .joined()
    }
}

extension String {

    func aesEncrypt(key: String) throws -> String {
        let iv : Array<UInt8> = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

        var result = ""

        do {

            let key: [UInt8] = Array(key.utf8) as [UInt8]
            let aes = try! AES(key: key, blockMode: CBC(iv: iv), padding: .noPadding) // AES128 .ECB pkcs7
            let encrypted = try aes.encrypt(Array(self.utf8))

            result = encrypted.toBase64()

            print("AES Encryption Result: \(result)")

        } catch {

            print("Error: \(error)")
        }

        return result
    }

    func aesDecrypt(key: String) throws -> String {
        let iv : Array<UInt8> = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        var result = ""

        do {

            let encrypted = self
            let key: [UInt8] = Array(key.utf8) as [UInt8]
            let aes = try! AES(key: key, blockMode: CBC(iv: iv), padding: .pkcs5) // AES128 .ECB pkcs7
            let decrypted = try aes.decrypt(Array(base64: encrypted))

            result = String(data: Data(decrypted), encoding: .utf8) ?? ""

            print("AES Decryption Result: \(result)")

        } catch {

            print("Error: \(error)")
        }

        return result
    }
}


extension String {
       enum ExtendedEncoding {
           case hexadecimal
       }

       func data(using encoding:ExtendedEncoding) -> Data? {
           let hexStr = self.dropFirst(self.hasPrefix("0x") ? 2 : 0)

           guard hexStr.count % 2 == 0 else { return nil }

           var newData = Data(capacity: hexStr.count/2)

           var indexIsEven = true
           for i in hexStr.indices {
               if indexIsEven {
                   let byteRange = i...hexStr.index(after: i)
                   guard let byte = UInt8(hexStr[byteRange], radix: 16) else { return nil }
                   newData.append(byte)
               }
               indexIsEven.toggle()
           }
           return newData
       }
   }

extension StringProtocol {
    var hexaData: Data { .init(hexa) }
    var hexaBytes: [UInt8] { .init(hexa) }
    private var hexa: UnfoldSequence<UInt8, Index> {
        sequence(state: startIndex) { startIndex in
            guard startIndex < self.endIndex else { return nil }
            let endIndex = self.index(startIndex, offsetBy: 2, limitedBy: self.endIndex) ?? self.endIndex
            defer { startIndex = endIndex }
            return UInt8(self[startIndex..<endIndex], radix: 16)
        }
    }
}




struct SessionBle{
    var nonce1String = ""
    var nonce2String = ""
    
    var inversedNonces = ""
    var sessionKey = ""
    
    var calculatedBadgeID = ""
        // received nonced are received in hex string
    mutating func calculateSessionKey(receivedNonces: String?){
        
        print("receivedNonces", receivedNonces)
        guard let receivedNonce1String = receivedNonces?.prefix(8),
              let receivedNonce2String = receivedNonces?.dropFirst(8)
        else { return }
        print("nonce1",  String(describing: receivedNonce1String))
        self.nonce2String = String(describing: receivedNonce2String)
        print("nonce2" , nonce2String)
        self.inversedNonces = String(describing: receivedNonce2String) + String(describing: receivedNonce1String)
        print("inversedNonces", inversedNonces)
        sessionKey = PeripheralViewController.aesEncrypt(key: HardCodedData.walletKey2, dataString: inversedNonces)
    }
    
    mutating func encryptBadgeId() -> String{
        
        let secondNonce = nonce2String.prefix(12)
        print("secondNonce", secondNonce)
        let dataString = String(describing: secondNonce) + HardCodedData.badgeID.lowercased()
        
        calculatedBadgeID = PeripheralViewController.aesEncrypt(key: sessionKey, dataString: dataString)
        
        return calculatedBadgeID
    }
}


struct HardCodedData{

    static let contactlessID = "FB5BAD02"
    static let zoneID = "745A95FD9723"
    static let whitelist = "no"
    static let zoneMasterKey = "26452948404D635166546A576D5A7134"
        // iv key 128 bites a 0 // 16 bytes : octets
    
    static let walletID = "b13f26b336a19198cc0403b24bd37c23"


// A3.41883158
   // static let walletKey2 = "9323650a2050b0b1823ad9bcab9f4e7c"
    
    
    // A3.51991094
     static let walletKey2 = "c7237f14019284be75df61873c30bc1f"
    
    static let badgeID = "0000001A2B3C4D5E6FFF"
}

