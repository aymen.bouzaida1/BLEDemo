//
//  PeripheralBLE.swift
//  BLEDemo
//
//  Created by Aymen Bouzaida on 25/01/2022.
//

import Foundation
import CoreBluetooth

class PeripheralBLE: NSObject{
    
    var basePeripheral : CBPeripheral!
    var advertisedName : String?
    var rssiString : String!
    init(withPeripheral peripheral: CBPeripheral, advertisementData advertisementDictionary: [String : Any], RSSI : NSNumber) {
        super.init()

        basePeripheral = peripheral
        advertisedName = parseAdvertisementData(advertisementDictionary) ?? ""
        rssiString = RSSI.description
        print("RSSI ", RSSI.description)
        
    }
    
    private func parseAdvertisementData(_ advertisementDictionary: [String : Any]) -> String? {
        var advertisedName: String

        if let name = advertisementDictionary[CBAdvertisementDataLocalNameKey] as? String{
            advertisedName = name
        } else {
            advertisedName = "Unknown Device"
        }
        
        return advertisedName
    }
}
