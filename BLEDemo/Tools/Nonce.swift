//
//  Nonce.swift
//  BLEDemo
//
//  Created by Aymen Bouzaida on 02/03/2022.
//


import Foundation

private let ALLOWED_BYTES: [UInt8] = [
//  0           1           2           3           4           5
    0b00110000, 0b00110001, 0b00110010, 0b00110011, 0b00110100, 0b00110101,
//  6           7           8           9           A           B
    0b00110110, 0b00110111, 0b00111000, 0b00111001, 0b01000001, 0b01000010,
//  C           D           E           F           G           H
    0b01000011, 0b01000100, 0b01000101, 0b01000110
    
]

/// A random value of a specified length that is representable as an ASCII alpha-numeric string.
public struct Nonce: CustomStringConvertible {
     let bytes: [UInt8]
    
    /// Creates a new random `Nonce` of the specified length in bytes.
    ///
    /// - parameter length: The number of random bytes.
    public init(length: Int = 32) {
        var result: [UInt8] = Array(repeating: 0, count: length)
        for i in 0..<length {
            let offset = arc4random_uniform(UInt32(ALLOWED_BYTES.count))
            result[i] = ALLOWED_BYTES[Int(offset)]
        }
        bytes = result
    }
    
    /// A human-readable representation of the underlying bytes as ASCII.
    public var description: String {
        return String(bytes: bytes, encoding: .ascii)!
    }
    
    /// The number of random bytes.
    public var length: Int {
        return bytes.count
    }
    
    /// The raw value of random bytes.
    public var data: Data {
        return Data(bytes: bytes)
    }
    
    /// The random bytes represented in hexadecimal notation.
    public var hexString: String {
        return bytes.hexString
    }
}

extension Sequence where Element == UInt8 {
    public var hexString: String {
        var hexadecimalString = ""
        var iterator = makeIterator()
        while let value = iterator.next() {
            hexadecimalString += String(format: "%02x", value)
        }
        return hexadecimalString
    }
}
