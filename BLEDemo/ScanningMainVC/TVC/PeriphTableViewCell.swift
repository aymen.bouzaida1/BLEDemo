//
//  PeriphTableViewCell.swift
//  BLEDemo
//
//  Created by Aymen Bouzaida on 25/01/2022.
//

import UIKit

class PeriphTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    private var lastUpdateTimestamp = Date()
    var currentPeriph: PeripheralBLE!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    public func setupView(withPeripheral aPeripheral: PeripheralBLE) {
        currentPeriph = aPeripheral
        titleLabel.text = (aPeripheral.advertisedName ?? "") + "       " + aPeripheral.rssiString
    }
    
    public func peripheralUpdatedAdvertisementData(_ aPeripheral: PeripheralBLE) {
        if Date().timeIntervalSince(lastUpdateTimestamp) > 1.0 {
            lastUpdateTimestamp = Date()
            setupView(withPeripheral: aPeripheral)
        }
    }
}
