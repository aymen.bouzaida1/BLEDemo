//
//  ViewController.swift
//  BLEDemo
//
//  Created by Aymen Bouzaida on 25/01/2022.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController{
    
    //MARK: - Outlets
    @IBOutlet weak var topBLEIcon: UIImageView!
    @IBOutlet weak var perriphTableView: UITableView!
    @IBOutlet weak var topBLEstate: UILabel!

    //MARK: - Variables
    private var centralManager : CBCentralManager!
    private var bluefruitPeripheral: CBPeripheral!

    var isBluetoothEnabled = false
    let bluetoothWarningMSG = "Enable Bluetooth to be able to scan for devices, tap me to proceed"
    let bluetoothEnabledMSG = "Scanning ..."
    
    var discoveredPeripherals = [PeripheralBLE]()

    var connectedPeripheral: PeripheralBLE?
  
    //MARK: - Outlets

    override func viewDidLoad() {
        super.viewDidLoad()
        
        centralManager = CBCentralManager(delegate: self, queue: nil, options: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        discoveredPeripherals.removeAll()
        perriphTableView.reloadData()
        
        if let connectedPeripheral = connectedPeripheral{
            centralManager.cancelPeripheralConnection(connectedPeripheral.basePeripheral)
        }

        if centralManager.state == .poweredOn {
            print("Bluetooth is On")
            isBluetoothEnabled = true
            topBLEstate.text = bluetoothEnabledMSG

            startScanning()
        }else {
            print("Bluetooth is not active")
            isBluetoothEnabled = false
            topBLEstate.text = bluetoothWarningMSG

            discoveredPeripherals.removeAll()
            perriphTableView.reloadData()
            _ = CBCentralManager(delegate: nil, queue: nil, options: [CBCentralManagerOptionShowPowerAlertKey:true])
        }
    }
    
    func startScanning() -> Void {
      // Start Scanning
//      centralManager?.scanForPeripherals(withServices: [CBUUIDs.BLEService_UUID])
        if centralManager.isScanning == true{
            return
        }
      centralManager.scanForPeripherals(withServices: nil, options: nil)

    }
    
    func connect(peripheralBle: PeripheralBLE) {
        centralManager.connect(peripheralBle.basePeripheral, options: nil)
    }
    
    func goToPeripheralVC(peripheralBLE: PeripheralBLE){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PeripheralVCID") as! PeripheralViewController
        vc.peripheralBLE = peripheralBLE
        
        self.navigationController?.show(vc, sender: self)
    }
  
    @IBAction func didTapProceedLabelOrView(_ sender: Any) {
        if isBluetoothEnabled{
            return
        }
        _ = CBCentralManager(delegate: nil, queue: nil, options: [CBCentralManagerOptionShowPowerAlertKey:true])
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discoveredPeripherals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = perriphTableView.dequeueReusableCell(withIdentifier: "PeriphTVCID", for: indexPath) as! PeriphTableViewCell
        let periph = discoveredPeripherals[indexPath.row]
        cell.setupView(withPeripheral: periph)
        
        return cell 
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedPeriph = discoveredPeripherals[indexPath.row]

        connect(peripheralBle: selectedPeriph)
    }
}

extension ViewController:  CBCentralManagerDelegate{
   
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == .poweredOn {
            print("Bluetooth is On")
            isBluetoothEnabled = true
//            centralManager.scanForPeripherals(withServices: nil, options: nil)
            startScanning()
            topBLEstate.text = bluetoothEnabledMSG
        } else {
            isBluetoothEnabled = false
            topBLEstate.text = bluetoothWarningMSG

            discoveredPeripherals.removeAll()
            perriphTableView.reloadData()
            print("Bluetooth is not active")
            _ = CBCentralManager(delegate: nil, queue: nil, options: [CBCentralManagerOptionShowPowerAlertKey:true])
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("\nName   : \(peripheral.name ?? "(No name)")")
          print("RSSI   : \(RSSI)")
          for ad in advertisementData {
              print("AD Data: \(ad)")
              print("AD Data: \(ad)")
              print()
          }
    
        let newPeripheral = PeripheralBLE(withPeripheral: peripheral, advertisementData: advertisementData, RSSI: RSSI)

        if !discoveredPeripherals.contains(where: { element in
            element.basePeripheral == peripheral
        }) && newPeripheral.advertisedName != "Unknown Device" && newPeripheral.advertisedName == "DOM PackID" {
            let manData = advertisementData["kCBAdvDataManufacturerData"] as? Data
            let manDataHex = manData?.hexString
            print("mmmmmm", manData)

            discoveredPeripherals.append(newPeripheral)
            perriphTableView.reloadData()
        } else {
            if let index = discoveredPeripherals.firstIndex(of: newPeripheral) {
                if let aCell = perriphTableView.cellForRow(at: [0, index]) as? PeriphTableViewCell {
                    aCell.peripheralUpdatedAdvertisementData(newPeripheral)
                }
            }
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        // Successfully connected. Store reference to peripheral if not already done.
        guard let selectedPeripheral = discoveredPeripherals.first(where: { periphBle in
            return periphBle.basePeripheral == peripheral
        }) else {
            return
        }
                
        self.connectedPeripheral = selectedPeripheral
        centralManager.stopScan()
        goToPeripheralVC(peripheralBLE: selectedPeripheral)
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        // Handle error
        
        print("error", error.debugDescription)
    }
}

struct CBUUIDs{

    static let kBLEService_UUID = "6e400001-b5a3-f393-e0a9-e50e24dcca9e"
    static let kBLE_Characteristic_uuid_Tx = "6e400002-b5a3-f393-e0a9-e50e24dcca9e"
    static let kBLE_Characteristic_uuid_Rx = "6e400003-b5a3-f393-e0a9-e50e24dcca9e"

    static let BLEService_UUID = CBUUID(string: kBLEService_UUID)
    static let BLE_Characteristic_uuid_Tx = CBUUID(string: kBLE_Characteristic_uuid_Tx)//(Property = Write without response)
    static let BLE_Characteristic_uuid_Rx = CBUUID(string: kBLE_Characteristic_uuid_Rx)// (Property = Read/Notify)
}
